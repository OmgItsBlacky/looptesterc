#ifndef LOOPTESTERC_CSVFILECREATOR_HPP
#define LOOPTESTERC_CSVFILECREATOR_HPP

#include <stdio.h>

#include "LoopExecutor.h"

#define TIME_BUFFOR_SIZE 32
#define MAX_CSV_FILE_PATH_LENGTH 256
#define FILE_NAME "C_LOOP_TESTER_OUTPUT"

char *currentTimeAsString();

FILE *generateCSVFile(const char *path);

void generateCSVFileFromHarvestedData(const char *path, LoopExecutorHarvestedData **data, uint32_t number_of_tests);

#endif //LOOPTESTERC_CSVFILECREATOR_HPP
