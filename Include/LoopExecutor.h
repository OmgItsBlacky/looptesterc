#ifndef LOOPTESTERC_LOOPEXECUTOR_H
#define LOOPTESTERC_LOOPEXECUTOR_H

#include <stdint.h>
#include <sys/time.h>
#include <stdlib.h>

typedef struct TimeData
{
    struct timespec start_;
    struct timespec stop_;
    uint64_t execution_time_in_milliseconds_;
    uint64_t execution_time_in_nanoseconds_;
} TimeInterval;

typedef struct HarvestedData
{
    TimeInterval *executionTimes;
    double average_execution_time_in_milliseconds_;
    double average_execution_time_in_nanoseconds_;
    uint64_t loop_range_;
    uint64_t number_of_loop_executions_;
    uint64_t data_size_;
} LoopExecutorHarvestedData;

int * initializeData(uint64_t data_size);

void describeLoopExecutorHarvestedData(LoopExecutorHarvestedData* data);

LoopExecutorHarvestedData *loopExecutor(
        int* (*loop)(int *, ulong),
        int *data,
        uint64_t loop_range,
        uint64_t number_of_loop_executions,
        uint64_t data_size
        );

TimeInterval *initializeTimeIntervalContainer(uint64_t number_of_loop_executions);

LoopExecutorHarvestedData *initializeLoopExecutorHarvestedDataStructure(
        uint64_t number_of_loop_executions,
        uint64_t loop_range,
        uint64_t data_size
        );

#endif //LOOPTESTERC_LOOPEXECUTOR_H
