cmake_minimum_required(VERSION 3.12)
project(looptesterc C)

if(NOT CMAKE_BUILD_TYPE)
    set(CMAKE_BUILD_TYPE Release)
endif()

set(CMAKE_CXX_FLAGS "-Wall -Wextra")
set(CMAKE_CXX_FLAGS_DEBUG "-g -O0")
set(CMAKE_CXX_FLAGS_RELEASE "-g -O2")

set(CMAKE_C_STANDARD 11)

add_executable(looptesterc Source/main.c Include/LoopExecutor.h Source/LoopExecutor.c Source/CSVFileCreator.c Include/CSVFileCreator.h)