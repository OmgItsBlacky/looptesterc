#include <stdint.h>

#include "../Include/LoopExecutor.h"
#include "../Include/CSVFileCreator.h"

int* longForLoopBubble(int* data, ulong loopRange)
{
    ulong i = 0;
    int temporary = 0;

    for (; i < (loopRange - 1); i++)
    {
        if(data[i] < data[i+1])
        {
            temporary = data[i];
            data[i] = data[i+1];
            data[i+1] = temporary;
        }
    }

    return data;
}

int* longForLoopBubbleOptimized(int* data, ulong loopRange)
{
    ulong i = 0;

    int temporary_A = 0;
    int temporary_B = 0;

    for (; i < loopRange - 1; i++)
    {
        temporary_A = data[i];
        temporary_B = data[i+1];

        data[i] = temporary_B > temporary_A ? temporary_B : temporary_A;
        data[i+1] = temporary_B > temporary_A ? temporary_A : temporary_B;
    }

    return data;
}

int main()
{
    uint64_t data_size = (uint64_t)1e9;
    uint64_t number_of_loop_executions = 10;
    int *dataLongForLoopControl = initializeData(data_size);
    int *dataLongForLoopTernaryOne = initializeData(data_size);

    uint16_t number_of_tests = 3;
    LoopExecutorHarvestedData *results[number_of_tests];

    results[0] = loopExecutor(
            longForLoopBubble,
            dataLongForLoopControl,
            data_size - 1,
            number_of_loop_executions,
            data_size);

    results[1] = loopExecutor(
            longForLoopBubbleOptimized,
            dataLongForLoopTernaryOne,
            data_size - 1,
            number_of_loop_executions,
            data_size);

    generateCSVFileFromHarvestedData("./", results, number_of_tests);

    return 0;
}