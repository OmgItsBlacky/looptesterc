#include "../Include/CSVFileCreator.h"

#include <time.h>
#include <string.h>

char *currentTimeAsString()
{
    time_t raw_time;
    struct tm *time_info;
    time(&raw_time);
    time_info = localtime(&raw_time);
    char *current_time = asctime(time_info);
    sprintf(current_time, "%s", current_time + 4);

    char *time_formatted_string = (char *) calloc(TIME_BUFFOR_SIZE, sizeof(char));
    strncpy(time_formatted_string, current_time, 20);

    if (!time_formatted_string)
    {
        return NULL;
    }

    for (int i = 0; i < strlen(time_formatted_string) - 1; i++)
    {
        if (time_formatted_string[i] == ' ')
        {
            time_formatted_string[i] = '_';
        }
    }
    return time_formatted_string;
}

FILE *generateCSVFile(const char *path)
{
    char *time_formatted_string = currentTimeAsString();
    char result_file_path[MAX_CSV_FILE_PATH_LENGTH];
    if (time_formatted_string)
    {
        sprintf(result_file_path, "%s/%s_%s.csv", path, FILE_NAME, time_formatted_string);
        free(time_formatted_string);
    }

    FILE *result = fopen(result_file_path, "w");
    return result;
}

void generateCSVFileFromHarvestedData(const char *path, LoopExecutorHarvestedData **data, uint32_t number_of_tests)
{
    FILE *outputFile = generateCSVFile(path);
    fprintf(outputFile,
            "Loop executions,Loop range,Data size,Average time of single loop execution nanoseconds\n");
    for (uint32_t i = 0; i < number_of_tests; i++)
    {
        fprintf(outputFile, "%6ld,%12ld,%12ld,%12lf\n",
                data[i]->number_of_loop_executions_,
                data[i]->loop_range_,
                data[i]->data_size_,
                data[i]->average_execution_time_in_nanoseconds_);
    }

    fclose(outputFile);
}