#include "../Include/LoopExecutor.h"

#include <time.h>
#include <bits/time.h>
#include <stdio.h>

LoopExecutorHarvestedData * loopExecutor(int* (*loop)(int *, ulong), int *data, uint64_t loop_range,
                                         uint64_t number_of_loop_executions, uint64_t data_size)
{
    LoopExecutorHarvestedData *result = initializeLoopExecutorHarvestedDataStructure(number_of_loop_executions,
                                                                                     loop_range, data_size);

    for (uint64_t i = 0; i < number_of_loop_executions; i++)
    {
        clock_gettime(CLOCK_MONOTONIC_RAW, &(result->executionTimes[i].start_));
        loop(data, loop_range);
        clock_gettime(CLOCK_MONOTONIC_RAW, &(result->executionTimes[i].stop_));

        result->executionTimes[i].execution_time_in_nanoseconds_ = (uint64_t)
                                                                           (result->executionTimes[i].stop_.tv_sec -
                                                                            result->executionTimes[i].start_.tv_sec) *
                                                                   1000000000 +
                                                                   (result->executionTimes[i].stop_.tv_nsec -
                                                                    result->executionTimes[i].start_.tv_nsec);

        result->executionTimes[i].execution_time_in_milliseconds_ =
                result->executionTimes[i].execution_time_in_nanoseconds_ / 1000;
    }

    double average_time_milliseconds = 0;
    double average_time_nanoseconds = 0;

    for (uint64_t i = 0; i < number_of_loop_executions; i++)
    {
        average_time_milliseconds += result->executionTimes[i].execution_time_in_milliseconds_;
        average_time_nanoseconds += result->executionTimes[i].execution_time_in_nanoseconds_;
    }

    average_time_milliseconds /= number_of_loop_executions;
    average_time_nanoseconds /= number_of_loop_executions;

    result->average_execution_time_in_milliseconds_ = average_time_milliseconds;
    result->average_execution_time_in_nanoseconds_ = average_time_nanoseconds;

    return result;
}

TimeInterval *initializeTimeIntervalContainer(uint64_t number_of_loop_executions)
{
    if (number_of_loop_executions > 0)
    {
        return ((TimeInterval *) calloc(number_of_loop_executions, sizeof(TimeInterval)));
    }
    return NULL;
}

LoopExecutorHarvestedData *
initializeLoopExecutorHarvestedDataStructure(uint64_t number_of_loop_executions, uint64_t loop_range,
                                             uint64_t data_size)
{
    LoopExecutorHarvestedData *result = (LoopExecutorHarvestedData *) calloc(1, sizeof(LoopExecutorHarvestedData));
    result->number_of_loop_executions_ = number_of_loop_executions;
    result->loop_range_ = loop_range;
    result->data_size_ = data_size;
    result->executionTimes = initializeTimeIntervalContainer(number_of_loop_executions);
    return result;
}

int *initializeData(uint64_t data_size)
{
    if (data_size > 0)
    {
        int *result = (int *) calloc(data_size, sizeof(int));

        if(!result)
        {
            return NULL;
        }

        for (uint64_t i = 0; i < data_size; i++)
        {
            result[i] = (int) i;
        }

        return result;
    }

    return NULL;
}

void describeLoopExecutorHarvestedData(LoopExecutorHarvestedData *data)
{
    printf("Loop executions: %6ld \tLoop range: %12ld \tData size: %12ld "
           "\tAverage time of single loop execution: %12lf [ns] "
           "\tAverage time of single iteration of loop: %5lf [ns]\n\n",
           data->number_of_loop_executions_,
           data->loop_range_,
           data->data_size_,
           data->average_execution_time_in_nanoseconds_,
           data->average_execution_time_in_nanoseconds_ / data->loop_range_);
}
